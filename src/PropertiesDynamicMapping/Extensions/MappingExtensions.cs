﻿using System;
using PropertiesDynamicMapping.Interfaces;

namespace PropertiesDynamicMapping.Extensions
{
    /// <summary>
    /// Расширения для работы с Mapping'ом.
    /// </summary>
    static class MappingExtensions
    {
        /// <summary>
        /// Разделитель цепочки вызовов.
        /// </summary>
        internal const char ComplexPropertyNameSplitter = '.';

        /// <summary>
        /// Получение массива последовательности обращений к свойствам.
        /// </summary>
        /// <typeparam name="TObject">Объект, к свойствам которого происходит обращение.</typeparam>
        /// <param name="mapping">Mapping'а на свойство.</param>
        /// <returns>Массив последовательности обращений к свойствам.</returns>
        internal static string[] GetComplexPropertiesCallChain<TObject> ( this IMapping<TObject> mapping ) where TObject : class
        {
            if ( !mapping.IsComplex )
                throw new Exception( "Property not complex." );

            return mapping.ObjectProperty.Split( ComplexPropertyNameSplitter );
        }
    }
}