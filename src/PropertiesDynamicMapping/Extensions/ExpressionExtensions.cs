﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace PropertiesDynamicMapping.Extensions
{
    /// <summary>
    /// Расширения для работы с выражениями.
    /// </summary>
    static class ExpressionExtensions
    {
        /// <summary>
        /// "Раскручивание" выражения с обычным или сложным свойством в строку.
        /// </summary>
        /// <typeparam name="TObject">Объект, к свойствам которого происходит обращение.</typeparam>
        /// <typeparam name="TProperty">Возвращаемый тип конечного свойства.</typeparam>
        /// <param name="expression">Выражение для "раскручивания".</param>
        /// <returns>Последовательность вызовов свойств в строковом виде.</returns>
        internal static string UnrollExpression<TObject, TProperty> ( this Expression<Func<TObject, TProperty>> expression )
        {
            var currentExpression = expression.Body as MemberExpression;
            var stack = new Stack<string>();

            while ( currentExpression != null )
            {
                stack.Push( currentExpression.Member.Name );
                currentExpression = currentExpression.Expression as MemberExpression;
            }

            return string.Join( ".", stack );
        }
    }
}