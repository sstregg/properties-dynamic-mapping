﻿using System;
using System.Collections.Generic;
using System.Linq;
using PropertiesDynamicMapping.Interfaces;

namespace PropertiesDynamicMapping
{
    /// <summary>
    /// Mapping на объект.
    /// </summary>
    /// <typeparam name="TObject">Тип объекта.</typeparam>
    public class Map<TObject> : IMap<TObject> where TObject : class
    {
        private Dictionary<string, IMapping<TObject>> mPropertiesMap;
        private HashSet<IMapping<TObject>> mMapping;

        /// <summary>
        /// Создание Mapping'а на объект с указанием Mapping'а конкретных свойств.
        /// </summary>
        /// <param name="mapping">Список Mapping'ов на свойства.</param>
        public Map ( IEnumerable<IMapping<TObject>> mapping )
        {
            if ( mapping == null )
                throw new ArgumentNullException( "mapping" );
            if ( !mapping.Any() )
                throw new Exception( "Mapping can not be empty." );

            mMapping = new HashSet<IMapping<TObject>>( mapping );
            mPropertiesMap = mMapping.ToDictionary( x => x.PseudoProperty );
        }

        public IEnumerable<IMapping<TObject>> Mapping { get { return mMapping; } }

        public object GetValue ( string pseudoProperty, TObject @object )
        {
            return mPropertiesMap[pseudoProperty].GetValue( @object );
        }

        public void SetValue<TValue> ( string pseudoProperty, TObject @object, TValue value )
        {
            mPropertiesMap[pseudoProperty].SetValue( @object, value );
        }
    }
}