﻿using System.Collections.Generic;

namespace PropertiesDynamicMapping.Interfaces
{
    /// <summary>
    /// Mapping на объект.
    /// </summary>
    /// <typeparam name="TObject">Тип объекта.</typeparam>
    public interface IMap<in TObject> where TObject : class
    {
        /// <summary>
        /// Список всех Mapping'ов на объект.
        /// </summary>
        IEnumerable<IMapping<TObject>> Mapping { get; }

        /// <summary>
        /// Получение значения свойства.
        /// </summary>
        /// <param name="pseudoProperty">Название псевдосвойства.</param>
        /// <param name="object">Объект, из которого нужно прочитать свойство.</param>
        /// <returns>Значение свойства.</returns>
        object GetValue ( string pseudoProperty, TObject @object );

        /// <summary>
        /// Задание значения свойству.
        /// </summary>
        /// <typeparam name="TValue">Тип значения.</typeparam>
        /// <param name="pseudoProperty">Название псевдосвойства.</param>
        /// <param name="object">Объект, из которого нужно прочитать свойство.</param>
        /// <param name="value">Значение свойства.</param>
        void SetValue<TValue> ( string pseudoProperty, TObject @object, TValue value );
    }
}