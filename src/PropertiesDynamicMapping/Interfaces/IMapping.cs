﻿namespace PropertiesDynamicMapping.Interfaces
{
    /// <summary>
    /// Mapping на свойство объекта.
    /// </summary>
    /// <typeparam name="TObject">Тип объекта к свойствам которого будет происходить обращение.</typeparam>
    public interface IMapping<in TObject> where TObject : class
    {
        /// <summary>
        /// Свойство объекта, на которое осуществляется Mapping.
        /// </summary>
        string ObjectProperty { get; }

        /// <summary>
        /// Псевдо свойство, по которому происходит обращение к оригинальному свойству объекта.
        /// </summary>
        string PseudoProperty { get; }

        /// <summary>
        /// Сложное обращение к свойству или нет. Сложное обращение к свойству - это последовательно обращение к цепочке свойств.
        /// </summary>
        bool IsComplex { get; }

        /// <summary>
        /// В цепочке свойств могут быть свойства которые могут вернуть Null, 
        /// в случае если значение True, то нужно обрабатывать такие ситуации 
        /// нормально и возвращать значение по умолчанию.
        /// </summary>
        bool IsNullable { get; }

        /// <summary>
        /// Получение значения свойства.
        /// </summary>
        /// <param name="object">Объект из которого нужно получить значение свойства.</param>
        /// <returns>Значение свойства.</returns>
        object GetValue ( TObject @object );

        /// <summary>
        /// Установка значения свойства.
        /// </summary>
        /// <param name="object">Объект в котором нужно установить значение свойства.</param>
        /// <param name="value">Значение.</param>
        void SetValue ( TObject @object, object value );
    }

    /// <summary>
    /// Mapping на свойство объекта с типипзацией значения свойства.
    /// </summary>
    /// <typeparam name="TObject">Тип объекта к свойствам которого будет происходить обращение.</typeparam>
    /// <typeparam name="TValue">Тип значения.</typeparam>
    public interface IMapping<in TObject, TValue> : IMapping<TObject> where TObject : class
    {
        /// <summary>
        /// Получение типизированного значения свойства.
        /// </summary>
        /// <param name="object">Объект из которого нужно получить значение свойства.</param>
        /// <returns>Значение свойства.</returns>
        TValue GetValueDirect ( TObject @object );

        /// <summary>
        /// Установка типизированного значения свойства.
        /// </summary>
        /// <param name="object">Объект в котором нужно установить значение свойства.</param>
        /// <param name="value">Типизированное значение.</param>
        void SetValueDirect ( TObject @object, TValue value );
    }
}