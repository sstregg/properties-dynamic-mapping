﻿using System;
using System.Linq.Expressions;
using PropertiesDynamicMapping.Extensions;
using PropertiesDynamicMapping.Factories;
using PropertiesDynamicMapping.Interfaces;

namespace PropertiesDynamicMapping
{
    /// <summary>
    /// Реализация Mapping'а на свойство объекта.
    /// </summary>
    /// <typeparam name="TObject">Тип объекта к свойствам которого будет происходить обращение.</typeparam>
    public class Mapping<TObject> : IMapping<TObject> where TObject : class
    {
        private Func<TObject, object> mGetter;
        private Action<TObject, object> mSetter;

        /// <summary>
        /// Создает Mapping'а на свойство объекта.
        /// </summary>
        /// <param name="objectProperty">Свойство объекта на которое создается Mapping.</param>
        /// <param name="pseudoProperty">Псевдосвойство, через которое будет происходть обращение к оригинальному свойству.</param>
        /// <param name="nullable">Учитывать, что в цепочке свойств могут быть свойства, которые могут вернут Null.</param>
        /// <param name="settings">Настройки Mapping'а.</param>
        public Mapping ( string objectProperty, string pseudoProperty, bool nullable = false, MappingSettings settings = null )
        {
            if ( objectProperty == null )
                throw new ArgumentNullException( "objectProperty" );
            if ( pseudoProperty == null )
                throw new ArgumentNullException( "pseudoProperty" );

            ObjectProperty = objectProperty;
            PseudoProperty = pseudoProperty;
            IsComplex = ObjectProperty.IndexOf( MappingExtensions.ComplexPropertyNameSplitter ) != -1;
            IsNullable = IsComplex && nullable; // nullable может быть только когда свойство сложное

            if ( settings == null )
                settings = MappingSettings.Default;

            if ( settings.MapGetter )
                mGetter = CreateGetterDelegate();
            if ( settings.MapSetter )
                mSetter = CreateSetterDelegate();
        }

        /// <summary>
        /// Создание Getter'а.
        /// </summary>
        /// <returns></returns>
        protected virtual Func<TObject, object> CreateGetterDelegate ( )
        {
            return DelegateMappingFactory<TObject>.CreateGetter<object>( this );
        }

        /// <summary>
        /// Создание Setter'а.
        /// </summary>
        /// <returns></returns>
        protected virtual Action<TObject, object> CreateSetterDelegate ( )
        {
            return DelegateMappingFactory<TObject>.CreateSetter<object>( this );
        }

        public string ObjectProperty { get; private set; }

        public string PseudoProperty { get; private set; }

        public bool IsComplex { get; private set; }

        public bool IsNullable { get; private set; }

        public object GetValue ( TObject @object )
        {
            return mGetter( @object );
        }

        public void SetValue ( TObject @object, object value )
        {
            mSetter( @object, value );
        }

        public override bool Equals ( object obj )
        {
            var mapping = obj as IMapping<TObject>;
            if ( mapping == null )
                return false;

            return mapping.ObjectProperty == ObjectProperty && mapping.PseudoProperty == PseudoProperty
                && mapping.IsNullable == IsNullable;
        }

        public override int GetHashCode ( )
        {
            int hash = 17;
            hash = hash * 23 + ObjectProperty.GetHashCode();
            hash = hash * 23 + PseudoProperty.GetHashCode();
            hash = hash * 23 + IsNullable.GetHashCode();
            return hash;
        }

        /// <summary>
        /// Создает Mapping'а на свойство объекта через задание выражения с указанием свойства.
        /// </summary>
        /// <typeparam name="TPropertyType">Тип свойства.</typeparam>
        /// <param name="objectProperty">Выражение задающее последовательность обращений к свойствам.</param>
        /// <param name="pseudoProperty">Псевдосвойство, через которое будет происходть обращение к оригинальному свойству.</param>
        /// <param name="nullable">Учитывать, что в цепочке свойств могут быть свойства, которые могут вернут Null.</param>
        /// <param name="settings">Настройки Mapping'а.</param>
        /// <returns></returns>
        public static Mapping<TObject> Create<TPropertyType> (
            Expression<Func<TObject, TPropertyType>> objectProperty,
            string pseudoProperty,
            bool nullable = false,
            MappingSettings settings = null
            )
        {
            var propertyName = objectProperty.UnrollExpression();
            return new Mapping<TObject>( propertyName, pseudoProperty, nullable, settings );
        }
    }

    public class Mapping<TObject, TPropertyType> : IMapping<TObject, TPropertyType> where TObject : class
    {
        private Func<TObject, TPropertyType> mGetter;
        private Action<TObject, TPropertyType> mSetter;

        /// <summary>
        /// Создает Mapping'а на свойство объекта.
        /// </summary>
        /// <param name="objectProperty">Свойство объекта на которое создается Mapping.</param>
        /// <param name="pseudoProperty">Псевдосвойство, через которое будет происходть обращение к оригинальному свойству.</param>
        /// <param name="nullable">Учитывать, что в цепочке свойств могут быть свойства, которые могут вернут Null.</param>
        /// <param name="settings">Настройки Mapping'а.</param>
        public Mapping ( string objectProperty, string pseudoProperty, bool nullable = false, MappingSettings settings = null )
        {
            ObjectProperty = objectProperty;
            PseudoProperty = pseudoProperty;
            IsComplex = ObjectProperty.IndexOf( MappingExtensions.ComplexPropertyNameSplitter ) != -1;
            IsNullable = IsComplex && nullable; // nullable может быть только когда Complex

            if ( settings == null )
                settings = MappingSettings.Default;

            if ( settings.MapGetter )
                mGetter = CreateGetterDelegate();
            if ( settings.MapSetter )
                mSetter = CreateSetterDelegate();
        }

        /// <summary>
        /// Создание Getter'а.
        /// </summary>
        /// <returns></returns>
        protected virtual Func<TObject, TPropertyType> CreateGetterDelegate ( )
        {
            return DelegateMappingFactory<TObject>.CreateGetter<TPropertyType>( this );
        }

        /// <summary>
        /// Создание Setter'а.
        /// </summary>
        /// <returns></returns>
        protected virtual Action<TObject, TPropertyType> CreateSetterDelegate ( )
        {
            return DelegateMappingFactory<TObject>.CreateSetter<TPropertyType>( this );
        }

        public string ObjectProperty { get; private set; }

        public string PseudoProperty { get; private set; }

        public bool IsComplex { get; private set; }

        public bool IsNullable { get; private set; }

        public object GetValue ( TObject @object )
        {
            return mGetter( @object );
        }

        public void SetValue ( TObject @object, object value )
        {
            mSetter( @object, (TPropertyType)value );
        }

        public TPropertyType GetValueDirect ( TObject @object )
        {
            return mGetter( @object );
        }

        public void SetValueDirect ( TObject @object, TPropertyType value )
        {
            mSetter( @object, value );
        }

        /// <summary>
        /// Создает Mapping'а на свойство объекта через задание выражения с указанием свойства.
        /// </summary>
        /// <param name="objectProperty">Выражение задающее последовательность обращений к свойствам.</param>
        /// <param name="pseudoProperty">Псевдосвойство, через которое будет происходть обращение к оригинальному свойству.</param>
        /// <param name="nullable">Учитывать, что в цепочке свойств могут быть свойства, которые могут вернут Null.</param>
        /// <param name="settings">Настройки Mapping'а.</param>
        /// <returns></returns>
        public static Mapping<TObject, TPropertyType> Create (
            Expression<Func<TObject, TPropertyType>> objectProperty,
            string pseudoProperty,
            bool nullable = false,
            MappingSettings settings = null
            )
        {
            var propertyName = objectProperty.UnrollExpression();
            return new Mapping<TObject, TPropertyType>( propertyName, pseudoProperty, nullable, settings );
        }
    }
}