﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using PropertiesDynamicMapping.Extensions;
using PropertiesDynamicMapping.Interfaces;

namespace PropertiesDynamicMapping.Factories
{
    /// <summary>
    /// Фабрика для создания "делегатов для прямого доступа к свойствам".
    /// </summary>
    /// <typeparam name="TObject">Тип объекта, со свойствами которого будет происходить работа.</typeparam>
    internal static class DelegateMappingFactory<TObject> where TObject : class
    {
        #region : internal :

        /// <summary>
        /// Создание Getter'а для получения значения свойства.
        /// </summary>
        /// <typeparam name="TReturn">Тип возвращаемого значения.</typeparam>
        /// <param name="mapping">Mapping'а на свойство.</param>
        /// <returns></returns>
        internal static Func<TObject, TReturn> CreateGetter<TReturn> ( IMapping<TObject> mapping )
        {
            if ( mapping.IsComplex )
                return CreateComplexGetter<TReturn>( mapping );
            else
                return CreateSimpleGetter<TReturn>( mapping );
        }

        /// <summary>
        /// Создание Setter'а для задания значения свойству.
        /// </summary>
        /// <typeparam name="TValue">Тип задаваемого значения.</typeparam>
        /// <param name="mapping">Mapping'а на свойство.</param>
        /// <returns></returns>
        internal static Action<TObject, TValue> CreateSetter<TValue> ( IMapping<TObject> mapping )
        {
            if ( mapping.IsComplex )
                return CreateComplexSetter<TValue>( mapping );
            else
                return CreateSimpleSetter<TValue>( mapping );
        }

        #endregion

        #region : private :

        #region setter

        /// <summary>
        /// Создание простого (без цепочки вызовов) Setter'а для задания значения свойству.
        /// </summary>
        /// <typeparam name="TValue">Тип задаваемого значения.</typeparam>
        /// <param name="mapping">Mapping'а на свойство.</param>
        /// <returns>Возвращает делегат для доступа к Setter'у.</returns>
        private static Action<TObject, TValue> CreateSimpleSetter<TValue> ( IMapping<TObject> mapping )
        {
            var propInfo = typeof( TObject ).GetProperty( mapping.ObjectProperty, BindingFlags.Instance | BindingFlags.Public );
            if ( propInfo == null )
                throw new Exception( string.Format( "Property '{0}' not found.", mapping.ObjectProperty ) );

            // Проверяем тип передаваемого значения и тип свойства на соответствие
            CheckActualType<TValue>( propInfo.PropertyType );

            // Прототип: static void Name ( TObject object, TValue value )
            var method = new DynamicMethod(
                            name: GenerateDelegateName( mapping ),
                            returnType: typeof( void ),
                            parameterTypes: new[] { typeof( TObject ), typeof( TValue ) },
                            m: typeof( DelegateMappingFactory<> ).Module,
                            skipVisibility: true );

            var il = method.GetILGenerator();
            // Загружаем объект в стек
            il.Emit( OpCodes.Ldarg_0 );
            // Загружаем задаваемое значение в стек
            il.Emit( OpCodes.Ldarg_1 );
            // Если тип задаваемого значения - ссылочный, и тип свойства - значимый,
            // то выполняем приведение типов (unboxing)
            if ( !typeof( TValue ).IsValueType && propInfo.PropertyType.IsValueType )
                il.Emit( OpCodes.Unbox_Any, propInfo.PropertyType );
            // Получение Setter'а
            var setter = propInfo.GetSetMethod();
            // Устанавливаем значение
            il.EmitCall( setter.IsVirtual ? OpCodes.Callvirt : OpCodes.Call, setter, null );
            il.Emit( OpCodes.Ret );

            return (Action<TObject, TValue>)method.CreateDelegate( typeof( Action<TObject, TValue> ) );
        }

        /// <summary>
        /// Создание сложного (с цепочкой вызовов) Setter'а для задания значения свойству.
        /// </summary>
        /// <typeparam name="TValue">Тип задаваемого значения.</typeparam>
        /// <param name="mapping">Mapping'а на свойство.</param>
        /// <returns>Возвращает делегат для доступа к Setter'у.</returns>
        private static Action<TObject, TValue> CreateComplexSetter<TValue> ( IMapping<TObject> mapping )
        {
            if ( !mapping.IsComplex )
                throw new Exception( "Property not complex." );

            var properties = mapping.GetComplexPropertiesCallChain();
            // Проверяем корректность свойств и их наличие
            ValidatePropertiesChain( properties );

            // Прототип: static void MethodName ( TObject object, TValue value )
            var method = new DynamicMethod(
                            name: GenerateDelegateName( mapping ),
                            returnType: typeof( void ),
                            parameterTypes: new[] { typeof( TObject ), typeof( TValue ) },
                            m: typeof( DelegateMappingFactory<> ).Module,
                            skipVisibility: true );
            var il = method.GetILGenerator();
            // Генерируем IL
            if ( mapping.IsNullable )
                GenerateILForComplexNullableSetter<TValue>( il, properties );
            else
                GenerateILForComplexSetter<TValue>( il, properties );

            return (Action<TObject, TValue>)method.CreateDelegate( typeof( Action<TObject, TValue> ) );
        }

        /// <summary>
        /// Метод генерации IL для последовательного обращения к цепоче свойств без проверки на Null, 
        /// и задания значения конечному свойству.
        /// </summary>
        /// <typeparam name="TValue">Тип задаваемого значения.</typeparam>
        /// <param name="il">IL генератор.</param>
        /// <param name="properties">Массив свойств описывающих цепочку вызовов.</param>
        private static void GenerateILForComplexSetter<TValue> ( ILGenerator il, string[] properties )
        {
            var propInfo = default( PropertyInfo );
            var type = typeof( TObject );

            // Загружаем объект в стек
            il.Emit( OpCodes.Ldarg_0 );
            // Проходимся по всем свойствам кроме последнего
            for ( int i = 0; i < properties.Length - 1; ++i )
            {
                // Получаем описание текущего свойства
                propInfo = type.GetProperty( properties[i], BindingFlags.Instance | BindingFlags.Public );
                // Получение Getter'а
                var getter = propInfo.GetGetMethod();
                // Получение значения текущего свойства
                il.EmitCall( getter.IsVirtual ? OpCodes.Callvirt : OpCodes.Call, getter, null );
                // Тип следующего свойства в цепочке вызовов
                type = propInfo.PropertyType;
            }

            // Получаем информацию о Setter'е
            propInfo = type.GetProperty( properties.Last(), BindingFlags.Instance | BindingFlags.Public );
            // Проверяем тип передаваемого значения и тип свойства на соответствие
            CheckActualType<TValue>( propInfo.PropertyType );

            // Загружаем значение в стек
            il.Emit( OpCodes.Ldarg_1 );
            // Если тип задаваемого значения - ссылочный, и тип свойства - значимый,
            // то выполняем приведение типов (unboxing)
            if ( !typeof( TValue ).IsValueType && propInfo.PropertyType.IsValueType )
                il.Emit( OpCodes.Unbox_Any, propInfo.PropertyType );
            // Получение Setter'а
            var setter = propInfo.GetSetMethod();
            // Устанавливаем значение
            il.EmitCall( setter.IsVirtual ? OpCodes.Callvirt : OpCodes.Call, setter, null );
            il.Emit( OpCodes.Ret );
        }

        /// <summary>
        /// Метод генерации IL для последовательного обращения к цепоче свойств с проверкой на Null, 
        /// и задания значения конечному свойству если все объекты корректны.
        /// </summary>
        /// <typeparam name="TValue">Тип задаваемого значения.</typeparam>
        /// <param name="il">IL генератор.</param>
        /// <param name="properties">Массив свойств описывающих цепочку вызовов.</param>
        private static void GenerateILForComplexNullableSetter<TValue> ( ILGenerator il, string[] properties )
        {
            var propInfo = default( PropertyInfo );
            var type = typeof( TObject );

            // Метка для возвращения из метода если какое-то свойство вернуло null
            var exitLabel = il.DefineLabel();
            // Загружаем объект в стек
            il.Emit( OpCodes.Ldarg_0 );
            // Проходим по всем свойствам кроме последнего
            for ( int i = 0; i < properties.Length - 1; ++i )
            {
                // Получаем описание текущего свойства
                propInfo = type.GetProperty( properties[i], BindingFlags.Instance | BindingFlags.Public );
                // Получение Getter'а
                var getter = propInfo.GetGetMethod();
                // Получение значения текущего свойства
                il.EmitCall( getter.IsVirtual ? OpCodes.Callvirt : OpCodes.Call, getter, null );
                // Вызов крайнего свойства отличается от остальных:
                // * не нужно делать проверку возвращаемого значения на null
                // * нужно поместить значение в стек и вызвать Setter
                if ( i != (properties.Length - 1) && !propInfo.PropertyType.IsValueType )
                {
                    // Повторно копируем ссылку на значение, находящуюся в стеке в текущий момент
                    // нужно для проверки на null
                    il.Emit( OpCodes.Dup );
                    // Проверяем на null и прыгаем на метку в случае истины
                    il.Emit( OpCodes.Brfalse_S, exitLabel );
                }
                // Тип следующего свойства в цепочке вызовов
                type = propInfo.PropertyType;
            }

            // Получаем информацию о Setter'е
            propInfo = type.GetProperty( properties.Last(), BindingFlags.Instance | BindingFlags.Public );
            // Проверяем тип передаваемого значения и тип свойства на соответствие
            CheckActualType<TValue>( propInfo.PropertyType );

            // Загружаем значение в стек
            il.Emit( OpCodes.Ldarg_1 );
            // Если тип задаваемого значения - ссылочный, и тип свойства - значимый,
            // то выполняем приведение типов (unboxing)
            if ( !typeof( TValue ).IsValueType && propInfo.PropertyType.IsValueType )
                il.Emit( OpCodes.Unbox_Any, propInfo.PropertyType );
            // Получение Setter'а
            var setter = propInfo.GetSetMethod();
            // Устанавливаем значение
            il.EmitCall( setter.IsVirtual ? OpCodes.Callvirt : OpCodes.Call, setter, null );
            il.Emit( OpCodes.Ret );
            // Обработка прыжка на метку
            il.MarkLabel( exitLabel );
            // После прыжка у нас в стеке лежит null, нужно его выбросить из стека
            // чтобы не получить stackoverflow
            il.Emit( OpCodes.Pop );
            il.Emit( OpCodes.Ret );
        }

        #endregion

        #region getter

        /// <summary>
        /// Создание простого (без цепочки вызовов) Getter'а для получения значения свойства.
        /// </summary>
        /// <typeparam name="TReturn">Тип возвращаемого значения.</typeparam>
        /// <param name="mapping">Mapping'а на свойство.</param>
        /// <returns>Возвращает делегат для доступа к Getter'у.</returns>
        private static Func<TObject, TReturn> CreateSimpleGetter<TReturn> ( IMapping<TObject> mapping )
        {
            var propInfo = typeof( TObject ).GetProperty( mapping.ObjectProperty, BindingFlags.Instance | BindingFlags.Public );
            if ( propInfo == null )
                throw new Exception( string.Format( "Property '{0}' not found.", mapping.ObjectProperty ) );

            // Проверяем возвращаемый тип и тип свойства на соответствие
            CheckActualType<TReturn>( propInfo.PropertyType );

            // Прототип: static TReturn MethodName ( TObject object )
            var method = new DynamicMethod(
                            name: GenerateDelegateName( mapping ),
                            returnType: typeof( TReturn ),
                            parameterTypes: new[] { typeof( TObject ) },
                            m: typeof( DelegateMappingFactory<> ).Module,
                            skipVisibility: true );

            var il = method.GetILGenerator();
            // Помещаем объект в стек
            il.Emit( OpCodes.Ldarg_0 );
            // Получение Getter'а
            var getter = propInfo.GetGetMethod();
            // Получение значения текущего свойства
            il.EmitCall( getter.IsVirtual ? OpCodes.Callvirt : OpCodes.Call, getter, null );
            // Если возвращаемый тип - ссылочный, а свойство - значимый, то выполняем приведение типов (boxing)
            if ( !typeof( TReturn ).IsValueType && propInfo.PropertyType.IsValueType )
                il.Emit( OpCodes.Box, propInfo.PropertyType );
            il.Emit( OpCodes.Ret );

            return (Func<TObject, TReturn>)method.CreateDelegate( typeof( Func<TObject, TReturn> ) );
        }

        /// <summary>
        /// Создание сложного (с цепочкой вызовов) Getter'а для получения значения свойства.
        /// </summary>
        /// <typeparam name="TReturn">Тип возвращаемого значения.</typeparam>
        /// <param name="mapping">Mapping'а на свойство.</param>
        /// <returns>Возвращает делегат для доступа к Getter'у.</returns>
        private static Func<TObject, TReturn> CreateComplexGetter<TReturn> ( IMapping<TObject> mapping )
        {
            if ( !mapping.IsComplex )
                throw new Exception( "Property not complex." );

            var properties = mapping.GetComplexPropertiesCallChain();
            // Проверяем корректность свойств и их наличие
            ValidatePropertiesChain( properties );

            // Прототип: static TReturn MethodName ( TObject object )
            var method = new DynamicMethod(
                            name: GenerateDelegateName( mapping ),
                            returnType: typeof( TReturn ),
                            parameterTypes: new[] { typeof( TObject ) },
                            m: typeof( DelegateMappingFactory<> ).Module,
                            skipVisibility: true );

            var il = method.GetILGenerator();
            // Генерируем IL
            if ( mapping.IsNullable )
                GenerateILForComplexNullableGetter<TReturn>( il, properties );
            else
                GenerateILForComplexGetter<TReturn>( il, properties );

            return (Func<TObject, TReturn>)method.CreateDelegate( typeof( Func<TObject, TReturn> ) );
        }

        /// <summary>
        /// Метод генерации IL для последовательного обращения к цепоче свойств без проверки на Null, 
        /// и возвращение значения конечного свойства.
        /// </summary>
        /// <typeparam name="TReturn">Тип возвращаемого значения.</typeparam>
        /// <param name="il">IL генератор.</param>
        /// <param name="properties">Массив свойств описывающих цепочку вызовов.</param>
        private static void GenerateILForComplexGetter<TReturn> ( ILGenerator il, string[] properties )
        {
            var propInfo = default( PropertyInfo );
            var type = typeof( TObject );

            // Помещаем объект в стек
            il.Emit( OpCodes.Ldarg_0 );
            // Проходим по всем свойствам
            for ( int i = 0; i < properties.Length; ++i )
            {
                // Получаем описание текущего свойства
                propInfo = type.GetProperty( properties[i], BindingFlags.Instance | BindingFlags.Public );
                // Получение Getter'а
                var getter = propInfo.GetGetMethod();
                // Получение значения текущего свойства
                il.EmitCall( getter.IsVirtual ? OpCodes.Callvirt : OpCodes.Call, getter, null );
                // Тип следующего свойства в цепочке вызовов
                type = propInfo.PropertyType;
            }

            // Проверяем возвращаемый тип и тип свойства на соответствие
            CheckActualType<TReturn>( propInfo.PropertyType );
            // Если возвращаемый тип - ссылочный, а свойство - значимый, то выполняем приведение типов (boxing)
            if ( !typeof( TReturn ).IsValueType && propInfo.PropertyType.IsValueType )
                il.Emit( OpCodes.Box, propInfo.PropertyType );
            il.Emit( OpCodes.Ret );
        }

        /// <summary>
        /// Метод генерации IL для последовательного обращения к цепоче свойств с проверкой на Null, 
        /// и возвращение значения конечного свойства если все объекты корректны.
        /// </summary>
        /// <typeparam name="TReturn">Тип возвращаемого значения.</typeparam>
        /// <param name="il">IL генератор.</param>
        /// <param name="properties">Массив свойств описывающих цепочку вызовов.</param>
        private static void GenerateILForComplexNullableGetter<TReturn> ( ILGenerator il, string[] properties )
        {
            var propInfo = default( PropertyInfo );
            var type = typeof( TObject );

            // Метка на которую совершается переход в случае если одно из свойств в цепочке вызовов null
            var defaultValueLabel = il.DefineLabel();
            // Помещаем объект в стек
            il.Emit( OpCodes.Ldarg_0 );
            // Проходим по всем свойствам
            for ( int i = 0; i < properties.Length; ++i )
            {
                // Получаем описание текущего свойства
                propInfo = type.GetProperty( properties[i], BindingFlags.Instance | BindingFlags.Public );
                // Получение Getter'а
                var getter = propInfo.GetGetMethod();
                // Получение значения текущего свойства
                il.EmitCall( getter.IsVirtual ? OpCodes.Callvirt : OpCodes.Call, getter, null );
                // Вызов крайнего свойства отличается от остальных:
                // * не нужно делать проверку возвращаемого значения на null
                // * нужно поместить значение в стек и вызвать Getter
                if ( i != (properties.Length - 1) && !propInfo.PropertyType.IsValueType )
                {
                    // Если использовать Dup, то будет stackoverflow (проверено peverify)
                    // Генерируем для каждого значения, полученного от свойства, свою
                    // переменную и сохраняем значение там
                    // Данный способ никак не влияет на производительность
                    var loc = il.DeclareLocal( propInfo.PropertyType );
                    il.Emit( OpCodes.Stloc, loc );
                    il.Emit( OpCodes.Ldloc, loc );
                    il.Emit( OpCodes.Brfalse_S, defaultValueLabel );
                    il.Emit( OpCodes.Ldloc, loc );
                }
                // Тип следующего свойства в цепочке вызовов
                type = propInfo.PropertyType;
            }

            // Проверяем возвращаемый тип на корректность
            CheckActualType<TReturn>( propInfo.PropertyType );

            // Если возвращаемый тип - ссылочный, а свойство - значимый, то выполняем приведение типов (boxing)
            if ( !typeof( TReturn ).IsValueType && propInfo.PropertyType.IsValueType )
                il.Emit( OpCodes.Box, propInfo.PropertyType );
            il.Emit( OpCodes.Ret );

            // Метка обработки null и возвращения значения по умолчанию
            il.MarkLabel( defaultValueLabel );

            // Локальная переменная существует только когда используется значимый тип
            if ( propInfo.PropertyType.IsValueType )
            {
                // Объявляем переменную
                var localVar = il.DeclareLocal( propInfo.PropertyType );
                // Загружаем адрес переменной в стек
                il.Emit( localVar.LocalIndex > 255 ? OpCodes.Ldloca : OpCodes.Ldloca_S, localVar );
                // Инициализируем значение
                il.Emit( OpCodes.Initobj, localVar.LocalType );
                // Загружаем локальную переменную в стек
                il.Emit( OpCodes.Ldloc, localVar );
                // Выполняем boxing, если нужно
                if ( !typeof( TReturn ).IsValueType )
                    il.Emit( OpCodes.Box, localVar.LocalType );
            }
            else if ( !typeof( TReturn ).IsValueType && !propInfo.PropertyType.IsValueType )
            {
                // В случае ссылочного типа просто возвращаем null
                il.Emit( OpCodes.Ldnull );
            }
            else
                throw new Exception( "Undefined behavior." );

            il.Emit( OpCodes.Ret );
        }

        #endregion

        /// <summary>
        /// Генерация имени делегата.
        /// </summary>
        /// <param name="mapping">Mapping'а на свойство.</param>
        /// <returns>Возвращает имя делегата.</returns>
        private static string GenerateDelegateName ( IMapping<TObject> mapping )
        {
            return typeof( TObject ).Name + "_" + mapping.ObjectProperty + "_" + mapping.PseudoProperty;
        }

        /// <summary>
        /// Проверка типов на соответствие.
        /// </summary>
        /// <typeparam name="TExpected">Ожидаемый тип.</typeparam>
        /// <param name="actualType">Фактический тип.</param>
        private static void CheckActualType<TExpected> ( Type actualType )
        {
            var expectedType = typeof( TExpected );

            if ( expectedType == actualType )
                return;

            if ( expectedType.IsValueType && !actualType.IsValueType )
                throw new Exception(
                    string.Format(
                        "Expected type is value type ({0}), but actual is not value type ({1}.",
                        expectedType, actualType )
                    );

            if ( expectedType != typeof( object ) && actualType.IsValueType )
                throw new Exception(
                    string.Format(
                        "Can't implicit cast actual type to expected. Expected type is not an object ({0}), but actual type is value type ({1}).",
                        expectedType, actualType )
                    );

            if ( !expectedType.IsValueType && !actualType.IsValueType && !actualType.IsSubclassOf( expectedType ) )
                throw new Exception(
                    string.Format(
                        "Both expected type ({0}) and actual type ({1}) are reference types, but actual type is not subclass of expected.",
                        expectedType, actualType )
                    );
        }

        /// <summary>
        /// Проверка корректности цепочки вызовов свойств на наличие всех свойств и отсутствие 
        /// значимых типов от первого и до крайнего свойства в цепочке вызовов.
        /// </summary>
        /// <param name="properties">Цепочка вызовов свойств.</param>
        private static void ValidatePropertiesChain ( IEnumerable<string> properties )
        {
            var propertiesArr = properties.ToArray();

            if ( propertiesArr.Length <= 1 )
                throw new Exception( "This property mapping is not complex." );

            var type = typeof( TObject );

            for ( int i = 0; i < propertiesArr.Length; ++i )
            {
                var propInfo = type.GetProperty( propertiesArr[i], BindingFlags.Instance | BindingFlags.Public );
                // Свойство отсутствует
                if ( propInfo == null )
                    throw new Exception( string.Format( "Property '{0}' not found.", propertiesArr[i] ) );
                // Значимый тип в середине цепочки вызовов свойств не поддерживается
                if ( i != (propertiesArr.Length - 1) && propInfo.PropertyType.IsValueType )
                    throw new Exception( "Found value type in the middle of the call chain - not supported." );

                type = propInfo.PropertyType;
            }
        }

        #endregion
    }
}