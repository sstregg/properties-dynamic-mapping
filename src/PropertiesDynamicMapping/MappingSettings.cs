﻿using System;

namespace PropertiesDynamicMapping
{
    /// <summary>
    /// Настройки Mapping'а.
    /// </summary>
    public class MappingSettings
    {
        /// <summary>
        /// Настройки по умолчанию.
        /// </summary>
        public static readonly MappingSettings Default = new MappingSettings( true, true );

        /// <summary>
        /// Создание настроек с указанными свойствами.
        /// </summary>
        /// <param name="mapGetter">Создание Mapping'а на Getter свойства.</param>
        /// <param name="mapSetter">Создание Mapping'а на Setter свойства.</param>
        public MappingSettings ( bool mapGetter, bool mapSetter )
        {
            if ( !mapGetter && !mapSetter )
                throw new Exception( "Invalid settings: should be set at least one property - mapGetter or mapSetter" );

            MapGetter = mapGetter;
            MapSetter = mapSetter;
        }

        /// <summary>
        /// Создавать Mapping'а на Getter свойства или нет.
        /// </summary>
        public bool MapGetter { get; private set; }

        /// <summary>
        /// Создавать Mapping'а на Setter свойства или нет.
        /// </summary>
        public bool MapSetter { get; private set; }
    }
}