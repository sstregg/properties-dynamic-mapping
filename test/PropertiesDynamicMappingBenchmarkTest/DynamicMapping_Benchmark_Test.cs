﻿using System;
using System.Reflection;
using BenchmarkDotNet;
using BenchmarkDotNet.Tasks;
using PropertiesDynamicMapping;
using PropertiesDynamicMapping.Interfaces;

namespace PropertiesDynamicMappingBenchmarkTest
{
    class MappingReflection<TType> : Mapping<TType> where TType : class
    {
        private PropertyInfo mPropInfo;

        public MappingReflection ( string objectProperty, string pseudoName )
            : base( objectProperty, pseudoName, false )
        {
            mPropInfo = typeof( TType ).GetProperty( objectProperty );
        }

        protected override Func<TType, object> CreateGetterDelegate ( )
        {
            return entity => mPropInfo.GetValue( entity );
        }

        protected sealed override Action<TType, object> CreateSetterDelegate ( )
        {
            return null;
        }
    }

    [Task( platform: BenchmarkPlatform.X86, warmupIterationCount: 2, targetIterationCount: 5, processCount: 1 )]
    [Task( platform: BenchmarkPlatform.X64, warmupIterationCount: 2, targetIterationCount: 5, processCount: 1 )]
    [Task( platform: BenchmarkPlatform.AnyCpu, warmupIterationCount: 2, targetIterationCount: 5, processCount: 1 )]
    public class DynamicMapping_Benchmark_Test
    {
        class SubSubEntityTest
        {
            public string Value { get; set; }
        }

        class SubEntityTest
        {
            public SubSubEntityTest SubField { get; set; }
        }

        class EntityTest
        {
            public ushort Field1 { get; set; }

            public string Field2 { get; set; }

            public Guid Field3 { get; set; }

            public string Field4 { get; set; }

            public string Field5 { get; set; }

            public string Field6 { get; set; }

            public string Field7 { get; set; }

            public string Field8 { get; set; }

            public string Field9 { get; set; }

            public string Field10 { get; set; }

            public SubEntityTest SubEntity { get; set; }
        }

        private string[] PseudoNames = { "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11" };
        private string SelectedPseudoProperty = "F11";
        private string SelectedEntityProperty = "SubEntity.SubField.Value";

        private EntityTest mEntity;

        private Mapping<EntityTest> mMapping;
        private Mapping<EntityTest, string> mMappingDirect;
        private Mapping<EntityTest> mMappingNullable;
        private Mapping<EntityTest, string> mMappingDirectNullable;
        private IMap<EntityTest> mMap;
        private IMap<EntityTest> mMapNullable;

        private IMap<EntityTest> mMapReflection;
        private MappingReflection<EntityTest> mMappingReflection;

        public DynamicMapping_Benchmark_Test ( )
        {
            var rnd = new Random();
            mEntity = new EntityTest() {
                Field1 = (ushort)rnd.Next(),
                Field2 = "Name " + rnd.Next(),
                Field3 = Guid.NewGuid(),
                Field4 = Guid.NewGuid().ToString(),
                Field5 = Guid.NewGuid().ToString(),
                Field6 = Guid.NewGuid().ToString(),
                Field7 = Guid.NewGuid().ToString(),
                Field8 = Guid.NewGuid().ToString(),
                Field9 = Guid.NewGuid().ToString(),
                Field10 = Guid.NewGuid().ToString(),
                SubEntity = new SubEntityTest() { SubField = new SubSubEntityTest() { Value = Guid.NewGuid().ToString() } }
            };

            var readOnlySettings = new MappingSettings( true, false );
            mMap = new Map<EntityTest>( new IMapping<EntityTest>[]
            {
                Mapping<EntityTest>.Create(x => x.Field1,  PseudoNames[0]),
                Mapping<EntityTest>.Create(x => x.Field2,  PseudoNames[1]),
                Mapping<EntityTest>.Create(x => x.Field3,  PseudoNames[2]),
                Mapping<EntityTest>.Create(x => x.Field4,  PseudoNames[3]),
                Mapping<EntityTest>.Create(x => x.Field5,  PseudoNames[4]),
                Mapping<EntityTest>.Create(x => x.Field6,  PseudoNames[5]),
                Mapping<EntityTest>.Create(x => x.Field7,  PseudoNames[6]),
                Mapping<EntityTest>.Create(x => x.Field8,  PseudoNames[7]),
                Mapping<EntityTest>.Create(x => x.Field9,  PseudoNames[8]),
                Mapping<EntityTest>.Create(x => x.Field10, PseudoNames[9]),
                Mapping<EntityTest>.Create(x => x.SubEntity.SubField.Value.Length, PseudoNames[10], settings: readOnlySettings),
            } );

            mMapNullable = new Map<EntityTest>( new IMapping<EntityTest>[]
            {
                Mapping<EntityTest>.Create(x => x.Field1,  PseudoNames[0], true),
                Mapping<EntityTest>.Create(x => x.Field2,  PseudoNames[1], true),
                Mapping<EntityTest>.Create(x => x.Field3,  PseudoNames[2], true),
                Mapping<EntityTest>.Create(x => x.Field4,  PseudoNames[3], true),
                Mapping<EntityTest>.Create(x => x.Field5,  PseudoNames[4], true),
                Mapping<EntityTest>.Create(x => x.Field6,  PseudoNames[5], true),
                Mapping<EntityTest>.Create(x => x.Field7,  PseudoNames[6], true),
                Mapping<EntityTest>.Create(x => x.Field8,  PseudoNames[7], true),
                Mapping<EntityTest>.Create(x => x.Field9,  PseudoNames[8], true),
                Mapping<EntityTest>.Create(x => x.Field10, PseudoNames[9], true),
                Mapping<EntityTest>.Create(x => x.SubEntity.SubField.Value.Length, PseudoNames[10], true, readOnlySettings),
            } );

            mMapReflection = new Map<EntityTest>( new IMapping<EntityTest>[]
            {
                new MappingReflection<EntityTest>("Field1",  PseudoNames[0]),
                new MappingReflection<EntityTest>("Field2",  PseudoNames[1]),
                new MappingReflection<EntityTest>("Field3",  PseudoNames[2]),
                new MappingReflection<EntityTest>("Field4",  PseudoNames[3]),
                new MappingReflection<EntityTest>("Field5",  PseudoNames[4]),
                new MappingReflection<EntityTest>("Field6",  PseudoNames[5]),
                new MappingReflection<EntityTest>("Field7",  PseudoNames[6]),
                new MappingReflection<EntityTest>("Field8",  PseudoNames[7]),
                new MappingReflection<EntityTest>("Field9",  PseudoNames[8]),
                new MappingReflection<EntityTest>("Field10", PseudoNames[9]),
            } );

            mMapping = new Mapping<EntityTest>( SelectedEntityProperty, SelectedPseudoProperty );
            mMappingDirect = new Mapping<EntityTest, string>( SelectedEntityProperty, SelectedPseudoProperty );

            mMappingNullable = new Mapping<EntityTest>( SelectedEntityProperty, SelectedPseudoProperty, true );
            mMappingDirectNullable = new Mapping<EntityTest, string>( SelectedEntityProperty, SelectedPseudoProperty, true );

            mMappingReflection = new MappingReflection<EntityTest>( "Field4", "F4" );

            //var flag = (mMappingDirect.GetValueDirect( mEntity ) == mMapping.GetValue( mEntity ) && mMappingDirect.GetValueDirect( mEntity ) == mMappingReflection.GetValue( mEntity ))
            //           && (mMapping.GetValue( mEntity ) == mMappingReflection.GetValue( mEntity ))
            //           && (mMappingDirectNullable.GetValueDirect(mEntity) == mMappingNullable.GetValue(mEntity) && mMappingDirectNullable.GetValueDirect(mEntity) == mMappingReflection.GetValue( mEntity ) );

            //if ( !flag )
            //    throw new Exception( "Invalid return values." );
        }

        [Benchmark( "Query value using map (mapping dictionary) & reflection" )]
        public object QueryValueByMapReflection ( )
        {
            return mMapReflection.GetValue( "F4", mEntity );
        }

        [Benchmark( "Query value using mapping & reflection" )]
        public object QueryValueByMappingReflection ( )
        {
            return mMappingReflection.GetValue( mEntity );
        }

        [Benchmark( "Query value using map (mapping dictionary) & delegate" )]
        public object QueryValueByMapDelegate ( )
        {
            return mMap.GetValue( SelectedPseudoProperty, mEntity );
        }

        [Benchmark( "Query value using mapping & delegate" )]
        public object QueryValueByMappingDelegate ( )
        {
            return mMapping.GetValue( mEntity );
        }

        [Benchmark( "Query value using direct mapping & delegate" )]
        public string QueryValueByMappingDirectDelegate ( )
        {
            return mMappingDirect.GetValueDirect( mEntity );
        }

        [Benchmark( "Query nullable value using map (mapping dictionary) & delegate" )]
        public object QueryNullableValueByMapDelegate ( )
        {
            return mMapNullable.GetValue( SelectedPseudoProperty, mEntity );
        }

        [Benchmark( "Query nullable value using mapping & delegate" )]
        public object QueryNullableValueByMappingDelegate ( )
        {
            return mMappingNullable.GetValue( mEntity );
        }

        [Benchmark( "Query nullable value using direct mapping & delegate" )]
        public string QueryNullableValueByMappingDirectDelegate ( )
        {
            return mMappingDirectNullable.GetValueDirect( mEntity );
        }

        [Benchmark( "Query value using native property" )]
        public string QueryValueByNative ( )
        {
            return mEntity.SubEntity.SubField.Value;
        }
    }
}