﻿using System;
using BenchmarkDotNet;

namespace PropertiesDynamicMappingBenchmarkTest
{
    class Program
    {
        private class C_class
        {
            public Guid Value { get; set; }
            public string ValueRef { get; set; }
        }

        private struct B_struct
        {
            public C_class C_Class_Prop { get; set; }
        }

        private class B_class
        {
            public C_class C_Class_Prop { get; set; }
        }

        private class A_class
        {
            public B_struct B_Struct_Prop { get; set; }
            public B_class B_Class_Prop { get; set; }
        }

        static void Main ( string[] args )
        {
            var completitionSwitch = new BenchmarkCompetitionSwitch( new[]
            {
                typeof(DynamicMapping_Benchmark_Test)
            } );
            completitionSwitch.Run( args );
            Console.ReadKey();
        }

        static Guid CallValueTypeTest ( A_class entity )
        {
            if ( entity.B_Class_Prop != null )
                if ( entity.B_Class_Prop.C_Class_Prop != null )
                    return entity.B_Class_Prop.C_Class_Prop.Value;

            return default( Guid );
        }

        static object CallValueTypeObjectTest ( A_class entity )
        {
            if ( entity.B_Class_Prop != null )
                if ( entity.B_Class_Prop.C_Class_Prop != null )
                    return entity.B_Class_Prop.C_Class_Prop.Value;

            return default( Guid );
        }

        static string CallRefTypeTest ( A_class entity )
        {
            if ( entity.B_Class_Prop != null )
                if ( entity.B_Class_Prop.C_Class_Prop != null )
                    return entity.B_Class_Prop.C_Class_Prop.ValueRef;

            return default( string );
        }

        static object CallRefTypeObjectTest ( A_class entity )
        {
            if ( entity.B_Class_Prop != null )
                if ( entity.B_Class_Prop.C_Class_Prop != null )
                    return entity.B_Class_Prop.C_Class_Prop.ValueRef;

            return default( string );
        }

        static void ComplexSetterValueType ( A_class entity, object value )
        {
            entity.B_Class_Prop.C_Class_Prop.Value = (Guid)value;
        }

        static void ComplexSetterReferenceType ( A_class entity, object value )
        {
            entity.B_Class_Prop.C_Class_Prop.ValueRef = (string)value;
        }

        static void ComplexNullableSetterValueType ( A_class entity, object value )
        {
            if ( entity.B_Class_Prop != null )
                if ( entity.B_Class_Prop.C_Class_Prop != null )
                    entity.B_Class_Prop.C_Class_Prop.Value = (Guid)value;
        }

        static void ComplexNullableSetterReferenceType ( A_class entity, object value )
        {
            if ( entity.B_Class_Prop != null )
                if ( entity.B_Class_Prop.C_Class_Prop != null )
                    entity.B_Class_Prop.C_Class_Prop.ValueRef = (string)value;
        }

        static void ComplexNullableSetterReferenceTypeWithLocalArgs ( A_class entity, object value )
        {
            var b_class = entity.B_Class_Prop;
            if ( b_class != null )
            {
                var c_class = b_class.C_Class_Prop;
                if ( c_class != null )
                    c_class.ValueRef = (string)value;
            }
        }
    }
}
