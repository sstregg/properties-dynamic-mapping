﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PropertiesDynamicMapping;

namespace PropertiesDynamicMappingTest
{
    [TestClass]
    public class MappingTest
    {
        #region Infrastructure

        private class C_class
        {
            public Guid Value { get; set; }
            public string ValueRef { get; set; }
        }

        private struct B_struct
        {
            public C_class C_Class_Prop { get; set; }
        }

        private class B_class
        {
            public C_class C_Class_Prop { get; set; }
        }

        private class A_class
        {
            public B_struct B_Struct_Prop { get; set; }

            public B_class B_Class_Prop { get; set; }

            public MainClass InheritedTest { get; set; }

            public AbstractClass AbstractTest { get; set; }
        }


        private class MainClass
        {
            private string mVirtualProp;

            public virtual string VirtualProp
            {
                get { return mVirtualProp; }
                set { mVirtualProp = "MainClass_" + value; }
            }
        }

        private class MainClassOverride : MainClass
        {
            private string mVirtualPropEx;

            public override string VirtualProp
            {
                get { return mVirtualPropEx; }
                set { mVirtualPropEx = "MainClassOverride_" + value; }
            }
        }

        private abstract class AbstractClass
        {
            public abstract string AbstractProp { get; set; }
        }

        private class AbstractClassImpl1 : AbstractClass
        {
            private string mValue;

            public override string AbstractProp
            {
                get { return mValue; }
                set { mValue = "AbstractClassImpl1_" + value; }
            }
        }

        private class AbstractClassImpl2 : AbstractClass
        {
            private string mValue;

            public override string AbstractProp
            {
                get { return mValue; }
                set { mValue = "AbstractClassImpl2_" + value; }
            }
        }

        #endregion

        [TestMethod]
        public void AbstractTest ( )
        {
            // Getter
            var mapping0 = Mapping<A_class>.Create( a => a.AbstractTest.AbstractProp, "Alfa" );
            var value0 = Guid.NewGuid().ToString();

            var obj00 = new A_class() { AbstractTest = new AbstractClassImpl1() { AbstractProp = value0 } };
            var expected00 = new AbstractClassImpl1() { AbstractProp = value0 };
            Assert.AreEqual( expected00.AbstractProp, mapping0.GetValue( obj00 ) );

            var obj01 = new A_class() { AbstractTest = new AbstractClassImpl2() { AbstractProp = value0 } };
            var expected01 = new AbstractClassImpl2() { AbstractProp = value0 };
            Assert.AreEqual( expected01.AbstractProp, mapping0.GetValue( obj01 ) );

            // The same as InheritedTest
        }

        [TestMethod]
        public void InheritedTest ( )
        {
            // Getter
            var mapping0 = Mapping<A_class>.Create( a => a.InheritedTest.VirtualProp, "Alfa" );
            var value0 = Guid.NewGuid().ToString();

            var obj00 = new A_class() { InheritedTest = new MainClass() { VirtualProp = value0 } };
            var expected00 = new MainClass() { VirtualProp = value0 };
            Assert.AreEqual( expected00.VirtualProp, mapping0.GetValue( obj00 ) );

            var obj01 = new A_class() { InheritedTest = new MainClassOverride() { VirtualProp = value0 } };
            var expected01 = new MainClassOverride() { VirtualProp = value0 };
            Assert.AreEqual( expected01.VirtualProp, mapping0.GetValue( obj01 ) );

            // Nullable getter
            var mapping1 = Mapping<A_class>.Create( a => a.InheritedTest.VirtualProp, "Alfa", true );
            var value1 = Guid.NewGuid().ToString();

            var obj10 = new A_class() { InheritedTest = new MainClass() { VirtualProp = value1 } };
            var expected10 = new MainClass() { VirtualProp = value1 };
            Assert.AreEqual( expected10.VirtualProp, mapping1.GetValue( obj10 ) );

            var obj11 = new A_class() { InheritedTest = new MainClassOverride() { VirtualProp = value1 } };
            var expected11 = new MainClassOverride() { VirtualProp = value1 };
            Assert.AreEqual( expected11.VirtualProp, mapping1.GetValue( obj11 ) );

            // Nullable getter with null
            var mapping2 = Mapping<A_class>.Create( a => a.InheritedTest.VirtualProp.Length, "Alfa", true, new MappingSettings( true, false ) );

            var obj20 = new A_class();
            Assert.AreEqual( 0, mapping2.GetValue( obj20 ) );

            var obj21 = new A_class() { InheritedTest = new MainClassOverride() };
            Assert.AreEqual( 0, mapping2.GetValue( obj21 ) );

            // Setter
            var mapping3 = Mapping<A_class>.Create( a => a.InheritedTest.VirtualProp, "Alfa" );
            var value3 = Guid.NewGuid().ToString();

            var obj30 = new A_class() { InheritedTest = new MainClass() };
            var expected30 = new A_class() { InheritedTest = new MainClass() { VirtualProp = value3 } };
            mapping3.SetValue( obj30, value3 );
            Assert.AreEqual( expected30.InheritedTest.VirtualProp, obj30.InheritedTest.VirtualProp );
            Assert.AreEqual( expected30.InheritedTest.VirtualProp, mapping3.GetValue( obj30 ) );

            // Nullable setter
            var mapping4 = Mapping<A_class>.Create( a => a.InheritedTest.VirtualProp, "Alfa", true );
            var value4 = Guid.NewGuid().ToString();

            var obj40 = new A_class() { InheritedTest = new MainClass() };
            var expected40 = new A_class() { InheritedTest = new MainClass() { VirtualProp = value4 } };
            mapping4.SetValue( obj40, value4 );
            Assert.AreEqual( expected40.InheritedTest.VirtualProp, obj40.InheritedTest.VirtualProp );
            Assert.AreEqual( expected40.InheritedTest.VirtualProp, mapping4.GetValue( obj40 ) );

            // Nullable setter with null
            var mapping5 = Mapping<A_class>.Create( a => a.InheritedTest.VirtualProp, "Alfa", true );

            var obj50 = new A_class();
            mapping5.SetValue( obj50, "34234" );
            Assert.AreEqual( null, mapping5.GetValue( obj50 ) );
        }

        [TestMethod]
        public void EqualityTest ( )
        {
            // 
            var mapping0 = Mapping<A_class>.Create( a => a.B_Class_Prop, "F1" );
            var mapping1 = Mapping<A_class>.Create( a => a.B_Class_Prop, "F1" );
            Assert.AreEqual( mapping0, mapping1 );

            // different object properties
            mapping0 = Mapping<A_class>.Create( a => a.B_Class_Prop, "F1" );
            mapping1 = Mapping<A_class>.Create( a => a.B_Struct_Prop, "F1" );
            Assert.AreNotEqual( mapping0, mapping1 );

            // different pseudo properties
            mapping0 = Mapping<A_class>.Create( a => a.B_Class_Prop, "F1" );
            mapping1 = Mapping<A_class>.Create( a => a.B_Class_Prop, "F2" );
            Assert.AreNotEqual( mapping0, mapping1 );

            // if property not complex, nullable parametr not used
            mapping0 = Mapping<A_class>.Create( a => a.B_Class_Prop, "F1" );
            mapping1 = Mapping<A_class>.Create( a => a.B_Class_Prop, "F1", true );
            Assert.AreEqual( mapping0, mapping1 );

            // different complex properties with nullable
            mapping0 = Mapping<A_class>.Create( a => a.B_Class_Prop.C_Class_Prop, "F1" );
            mapping1 = Mapping<A_class>.Create( a => a.B_Class_Prop.C_Class_Prop, "F1", true );
            Assert.AreNotEqual( mapping0, mapping1 );

            // different TObject's
            var mapping2 = Mapping<A_class>.Create( a => a.B_Class_Prop, "F1" );
            var mapping3 = Mapping<B_class>.Create( a => a.C_Class_Prop, "F1" );
            Assert.AreNotEqual( mapping2, mapping3 );
        }

        [TestMethod]
        public void CreationTest ( )
        {
            var exceptionText = "objectProperty";

            try
            {
                new Mapping<A_class>( null, "F1" );
                Assert.Fail( "ArgumentNullException expected, param name: " + exceptionText );
            }
            catch ( ArgumentNullException ex )
            {
                Assert.AreEqual( exceptionText, ex.ParamName );
            }
            catch ( Exception ex )
            {
                Assert.Fail( "ArgumentNullException expected, but get other exception: " + ex.Message );
            }

            exceptionText = "pseudoProperty";

            try
            {
                new Mapping<A_class>( "B_Class_Prop", null );
                Assert.Fail( "ArgumentNullException expected, param name: " + exceptionText );
            }
            catch ( ArgumentNullException ex )
            {
                Assert.AreEqual( exceptionText, ex.ParamName );
            }
            catch ( Exception ex )
            {
                Assert.Fail( "ArgumentNullException expected, but get other exception: " + ex.Message );
            }
        }

        [TestMethod]
        public void CreationWithExpressionTest ( )
        {
            var expected = "B_Class_Prop";
            var mappingObject = Mapping<A_class>.Create( a => a.B_Class_Prop, "Alfa" );

            Assert.AreEqual( expected, mappingObject.ObjectProperty );

            expected = "B_Class_Prop.C_Class_Prop.ValueRef.Length";
            var mappingInt = Mapping<A_class, int>.Create( a => a.B_Class_Prop.C_Class_Prop.ValueRef.Length, "Alfa", settings: new MappingSettings( true, false ) );

            Assert.AreEqual( expected, mappingInt.ObjectProperty );
        }

        [TestMethod]
        public void ComplexPropertiesValidationTest ( )
        {
            var exceptionText = "Property 'NonExistentProperty' not found.";

            try
            {
                new Mapping<A_class>( "NonExistentProperty", "Alfa" );
                Assert.Fail( "Trying get access to nob-existent property. Exception expected: " + exceptionText );
            }
            catch ( Exception ex )
            {
                Assert.AreEqual( exceptionText, ex.Message );
            }

            try
            {
                new Mapping<A_class>( "B_Class_Prop.NonExistentProperty", "Alfa" );
                Assert.Fail( "Trying get access to nob-existent property. Exception expected: " + exceptionText );
            }
            catch ( Exception ex )
            {
                Assert.AreEqual( exceptionText, ex.Message );
            }
        }

        [TestMethod]
        public void NotSuitableTypesTest ( )
        {
            var exceptionText = "Both expected type (PropertiesDynamicMappingTest.MappingTest+MainClass) and actual type (PropertiesDynamicMappingTest.MappingTest+B_class) are reference types, but actual type is not subclass of expected.";

            try
            {
                new Mapping<A_class, MainClass>( "B_Class_Prop", "Alfa" );
                Assert.Fail( "Invalid reference types assignment. Exception expected: " + exceptionText );
            }
            catch ( Exception ex )
            {
                Assert.AreEqual( exceptionText, ex.Message );
            }

            exceptionText = "Can't implicit cast actual type to expected. Expected type is not an object (PropertiesDynamicMappingTest.MappingTest+MainClass), but actual type is value type (PropertiesDynamicMappingTest.MappingTest+B_struct).";

            try
            {
                new Mapping<A_class, MainClass>( "B_Struct_Prop", "Alfa" );
                Assert.Fail( "Invalid reference type to value type assignment. Exception expected: " + exceptionText );
            }
            catch ( Exception ex )
            {
                Assert.AreEqual( exceptionText, ex.Message );
            }

            exceptionText = "Expected type is value type (PropertiesDynamicMappingTest.MappingTest+B_struct), but actual is not value type (PropertiesDynamicMappingTest.MappingTest+B_class.";

            try
            {
                new Mapping<A_class, B_struct>( "B_Class_Prop", "Alfa" );
                Assert.Fail( "Invalid value type to value type assignment. Exception expected: " + exceptionText );
            }
            catch ( Exception ex )
            {
                Assert.AreEqual( exceptionText, ex.Message );
            }
        }

        [TestMethod]
        public void StructInTheMiddleTest ( )
        {
            var exceptionText = "Found value type in the middle of the call chain - not supported.";

            try
            {
                new Mapping<A_class>( "B_Struct_Prop.C_Class_Prop.Value", "Alfa" );

                Assert.Fail( "Exception expected: " + exceptionText );
            }
            catch ( Exception ex )
            {
                Assert.AreEqual( exceptionText, ex.Message );
            }
        }

        #region Simple setter

        [TestMethod]
        public void SimpleSetterValueTest ( )
        {
            object expected = Guid.NewGuid();
            var mapping = new Mapping<C_class>( "Value", "Alfa" );
            var obj = new C_class();
            mapping.SetValue( obj, expected );

            Assert.AreEqual( (Guid)expected, obj.Value );
        }

        [TestMethod]
        public void SimpleSetterReferenceTest ( )
        {
            object expected = "example-simple-setter-ref";
            var mapping = new Mapping<C_class>( "ValueRef", "Alfa" );
            var obj = new C_class();
            mapping.SetValue( obj, expected );

            Assert.AreEqual( (string)expected, obj.ValueRef );
        }

        [TestMethod]
        public void SimpleSetterDirectValueTest ( )
        {
            Guid expected = Guid.NewGuid();
            var mapping = new Mapping<C_class, Guid>( "Value", "Alfa" );
            var obj = new C_class();
            mapping.SetValueDirect( obj, expected );

            Assert.AreEqual( expected, obj.Value );
        }

        [TestMethod]
        public void SimpleSetterDirectReferenceTest ( )
        {
            string expected = "example-simple-setter-direct-ref";
            var mapping = new Mapping<C_class, string>( "ValueRef", "Alfa" );
            var obj = new C_class();
            mapping.SetValueDirect( obj, expected );

            Assert.AreEqual( expected, obj.ValueRef );
        }

        #endregion

        #region Complex setter

        [TestMethod]
        public void ComplexSetterValueTest ( )
        {
            object expected = Guid.NewGuid();
            var mapping = new Mapping<A_class>( "B_Class_Prop.C_Class_Prop.Value", "Alfa" );
            var obj = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() } };
            mapping.SetValue( obj, expected );

            Assert.AreEqual( (Guid)expected, obj.B_Class_Prop.C_Class_Prop.Value );
        }

        [TestMethod]
        public void ComplexSetterReferenceTest ( )
        {
            object expected = "example-complex-setter-ref";
            var mapping = new Mapping<A_class>( "B_Class_Prop.C_Class_Prop.ValueRef", "Alfa" );
            var obj = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() } };
            mapping.SetValue( obj, expected );

            Assert.AreEqual( expected, obj.B_Class_Prop.C_Class_Prop.ValueRef );
        }

        [TestMethod]
        public void ComplexSetterDirectValueTest ( )
        {
            Guid expected = Guid.NewGuid();
            var mapping = new Mapping<A_class, Guid>( "B_Class_Prop.C_Class_Prop.Value", "Alfa" );
            var obj = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() } };
            mapping.SetValueDirect( obj, expected );

            Assert.AreEqual( expected, obj.B_Class_Prop.C_Class_Prop.Value );
        }

        [TestMethod]
        public void ComplexSetterDirectReferenceTest ( )
        {
            string expected = "example-complex-setter-direct-ref";
            var mapping = new Mapping<A_class, string>( "B_Class_Prop.C_Class_Prop.ValueRef", "Alfa" );
            var obj = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() } };
            mapping.SetValueDirect( obj, expected );

            Assert.AreEqual( expected, obj.B_Class_Prop.C_Class_Prop.ValueRef );
        }

        [TestMethod]
        public void ComplexNullableSetterValueTest ( )
        {
            object expected = Guid.NewGuid();
            var mapping = new Mapping<A_class>( "B_Class_Prop.C_Class_Prop.Value", "Alfa", true );
            var obj = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() } };
            mapping.SetValue( obj, expected );

            Assert.AreEqual( expected, obj.B_Class_Prop.C_Class_Prop.Value );
        }

        [TestMethod]
        public void ComplexNullableSetterValueNullTest ( )
        {
            object expected = Guid.NewGuid();
            var mapping = new Mapping<A_class>( "B_Class_Prop.C_Class_Prop.Value", "Alfa", true );
            var obj = new A_class() { B_Class_Prop = new B_class() };
            mapping.SetValue( obj, expected );

            Assert.AreEqual( Guid.Empty, mapping.GetValue( obj ) );
        }

        [TestMethod]
        public void ComplexNullableSetterReferenceTest ( )
        {
            object expected = "example-complex-nullable-setter-ref";
            var mapping = new Mapping<A_class>( "B_Class_Prop.C_Class_Prop.ValueRef", "Alfa", true );
            var obj = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() } };
            mapping.SetValue( obj, expected );

            Assert.AreEqual( expected, obj.B_Class_Prop.C_Class_Prop.ValueRef );
        }

        [TestMethod]
        public void ComplexNullableSetterReferenceNullTest ( )
        {
            object expected = "example-complex-nullable-setter-ref";
            var mapping = new Mapping<A_class>( "B_Class_Prop.C_Class_Prop.ValueRef", "Alfa", true );
            var obj = new A_class() { B_Class_Prop = new B_class() };
            mapping.SetValue( obj, expected );

            Assert.AreEqual( null, mapping.GetValue( obj ) );
        }

        [TestMethod]
        public void ComplexNullableSetterDirectValueTest ( )
        {
            Guid expected = Guid.NewGuid();
            var mapping = new Mapping<A_class, Guid>( "B_Class_Prop.C_Class_Prop.Value", "Alfa", true );
            var obj = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() } };
            mapping.SetValueDirect( obj, expected );

            Assert.AreEqual( expected, obj.B_Class_Prop.C_Class_Prop.Value );
        }

        [TestMethod]
        public void ComplexNullableSetterDirectValueNullTest ( )
        {
            Guid expected = Guid.NewGuid();
            var mapping = new Mapping<A_class, Guid>( "B_Class_Prop.C_Class_Prop.Value", "Alfa", true );
            var obj = new A_class() { B_Class_Prop = new B_class() };
            mapping.SetValueDirect( obj, expected );

            Assert.AreEqual( Guid.Empty, mapping.GetValueDirect( obj ) );
        }

        [TestMethod]
        public void ComplexNullableSetterDirectReferenceTest ( )
        {
            string expected = "example-complex-nullable-setter-direct-ref";
            var mapping = new Mapping<A_class, string>( "B_Class_Prop.C_Class_Prop.ValueRef", "Alfa", true );
            var obj = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() } };
            mapping.SetValueDirect( obj, expected );

            Assert.AreEqual( expected, obj.B_Class_Prop.C_Class_Prop.ValueRef );
        }

        [TestMethod]
        public void ComplexNullableSetterDirectReferenceNullTest ( )
        {
            string expected = "example-complex-nullable-setter-direct-ref";
            var mapping = new Mapping<A_class, string>( "B_Class_Prop.C_Class_Prop.ValueRef", "Alfa", true );
            var obj = new A_class() { B_Class_Prop = new B_class() };
            mapping.SetValueDirect( obj, expected );

            Assert.AreEqual( null, mapping.GetValueDirect( obj ) );
        }

        #endregion

        #region Simple getter

        [TestMethod]
        public void SimpleGetterValueTest ( )
        {
            var mapping = new Mapping<C_class>( "Value", "Alfa" );
            var obj = new C_class() { Value = Guid.NewGuid() };
            var actual = mapping.GetValue( obj );

            Assert.AreEqual( obj.Value, actual );
        }

        [TestMethod]
        public void SimpleGetterReferenceTest ( )
        {
            var mapping = new Mapping<C_class>( "ValueRef", "Alfa" );
            var obj = new C_class() { ValueRef = "example-simple-getter-ref" };
            var actual = mapping.GetValue( obj );

            Assert.AreEqual( obj.ValueRef, actual );
        }

        [TestMethod]
        public void SimpleGetterDirectValueTest ( )
        {
            var mapping = new Mapping<C_class, Guid>( "Value", "Alfa" );
            var obj = new C_class() { Value = Guid.NewGuid() };
            var actual = mapping.GetValueDirect( obj );

            Assert.AreEqual( obj.Value, actual );
        }

        [TestMethod]
        public void SimpleGetterDirectReferenceTest ( )
        {
            var mapping = new Mapping<C_class, string>( "ValueRef", "Alfa" );
            var obj = new C_class() { ValueRef = "example-getter-direct-ref" };
            var actual = mapping.GetValueDirect( obj );

            Assert.AreEqual( obj.ValueRef, actual );
        }

        #endregion

        #region Complex getter

        [TestMethod]
        public void ComplexGetterValueTest ( )
        {
            var mapping = new Mapping<A_class>( "B_Class_Prop.C_Class_Prop.Value", "Alfa" );
            var a = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() { Value = Guid.NewGuid() } } };
            var actual = mapping.GetValue( a );

            Assert.AreEqual( a.B_Class_Prop.C_Class_Prop.Value, actual );
        }

        [TestMethod]
        public void ComplexGetterReferenceTest ( )
        {
            var mapping = new Mapping<A_class>( "B_Class_Prop.C_Class_Prop.ValueRef", "Alfa" );
            var a = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() { ValueRef = "example-getter-complex-ref" } } };
            var actual = mapping.GetValue( a );

            Assert.AreEqual( a.B_Class_Prop.C_Class_Prop.ValueRef, actual );
        }

        [TestMethod]
        public void ComplexGetterDirectValueTest ( )
        {
            var mapping = new Mapping<A_class, Guid>( "B_Class_Prop.C_Class_Prop.Value", "Alfa" );
            var a = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() { Value = Guid.NewGuid() } } };
            var actual = mapping.GetValueDirect( a );

            Assert.AreEqual( a.B_Class_Prop.C_Class_Prop.Value, actual );
        }

        [TestMethod]
        public void ComplexGetterDirectReferenceTest ( )
        {
            var mapping = new Mapping<A_class, string>( "B_Class_Prop.C_Class_Prop.ValueRef", "Alfa" );
            var a = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() { ValueRef = "example-getter-complex-direct-ref" } } };
            var actual = mapping.GetValueDirect( a );

            Assert.AreEqual( a.B_Class_Prop.C_Class_Prop.ValueRef, actual );
        }

        [TestMethod]
        public void ComplexNullableGetterValueTest ( )
        {
            var mapping = new Mapping<A_class>( "B_Class_Prop.C_Class_Prop.Value", "Alfa", true );
            var a = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() { Value = Guid.NewGuid() } } };
            var actual = mapping.GetValue( a );

            Assert.AreEqual( a.B_Class_Prop.C_Class_Prop.Value, actual );
        }

        [TestMethod]
        public void ComplexNullableGetterValueNullTest ( )
        {
            var mapping = new Mapping<A_class>( "B_Class_Prop.C_Class_Prop.Value", "Alfa", true );
            var a = new A_class() { B_Class_Prop = new B_class() };
            var actual = mapping.GetValue( a );

            Assert.AreEqual( Guid.Empty, actual );
        }

        [TestMethod]
        public void ComplexNullableGetterReferenceTest ( )
        {
            var mapping = new Mapping<A_class>( "B_Class_Prop.C_Class_Prop.ValueRef", "Alfa", true );
            var a = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() { ValueRef = "example-getter-complex-ref" } } };
            var actual = mapping.GetValue( a );

            Assert.AreEqual( a.B_Class_Prop.C_Class_Prop.ValueRef, actual );
        }

        [TestMethod]
        public void ComplexNullableGetterReferenceNullTest ( )
        {
            var mapping = new Mapping<A_class>( "B_Class_Prop.C_Class_Prop.ValueRef", "Alfa", true );
            var a = new A_class() { B_Class_Prop = new B_class() };
            var actual = mapping.GetValue( a );

            Assert.AreEqual( null, actual );
        }

        [TestMethod]
        public void ComplexNullableGetterDirectValueTest ( )
        {
            var mapping = new Mapping<A_class, Guid>( "B_Class_Prop.C_Class_Prop.Value", "Alfa", true );
            var a = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() { Value = Guid.NewGuid() } } };
            var actual = mapping.GetValueDirect( a );

            Assert.AreEqual( a.B_Class_Prop.C_Class_Prop.Value, actual );
        }

        [TestMethod]
        public void ComplexNullableGetterDirectValueNullTest ( )
        {
            var mapping = new Mapping<A_class, Guid>( "B_Class_Prop.C_Class_Prop.Value", "Alfa", true );
            var a = new A_class() { B_Class_Prop = new B_class() };
            var actual = mapping.GetValueDirect( a );

            Assert.AreEqual( Guid.Empty, actual );
        }

        [TestMethod]
        public void ComplexNullableGetterDirectReferenceTest ( )
        {
            var mapping = new Mapping<A_class, string>( "B_Class_Prop.C_Class_Prop.ValueRef", "Alfa", true );
            var a = new A_class() { B_Class_Prop = new B_class() { C_Class_Prop = new C_class() { ValueRef = "example-getter-complex-ref" } } };
            var actual = mapping.GetValueDirect( a );

            Assert.AreEqual( a.B_Class_Prop.C_Class_Prop.ValueRef, actual );
        }

        [TestMethod]
        public void ComplexNullableGetterDirectReferenceNullTest ( )
        {
            var mapping = new Mapping<A_class, string>( "B_Class_Prop.C_Class_Prop.ValueRef", "Alfa", true );
            var a = new A_class() { B_Class_Prop = new B_class() };
            var actual = mapping.GetValueDirect( a );

            Assert.AreEqual( null, actual );
        }

        #endregion
    }
}