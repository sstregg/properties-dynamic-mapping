﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PropertiesDynamicMapping;
using PropertiesDynamicMapping.Interfaces;

namespace PropertiesDynamicMappingTest
{
    [TestClass]
    public class MapTest
    {
        private class MyClass
        {
            public string ValueRef { get; set; }

            public Guid Value { get; set; }
        }

        [TestMethod]
        public void MapCreationTest ( )
        {
            try
            {
                new Map<MyClass>( null );
                Assert.Fail( "ArgumentNullException expected, param name: mapping." );
            }
            catch ( ArgumentNullException ex )
            {
                Assert.AreEqual( "mapping", ex.ParamName );
            }

            var exceptionText = "Mapping can not be empty.";

            try
            {
                new Map<MyClass>( new Mapping<MyClass>[0] );
                Assert.Fail( "Exception expected: " + exceptionText );
            }
            catch ( Exception ex )
            {
                Assert.AreEqual( exceptionText, ex.Message );
            }
        }

        [TestMethod]
        public void MapGetterTest ( )
        {
            var map = new Map<MyClass>( new IMapping<MyClass>[]
            {
                new Mapping<MyClass>("ValueRef", "F1"),
                new Mapping<MyClass>("Value", "F2"),
                new Mapping<MyClass, int>("ValueRef.Length", "F3", true, new MappingSettings(true, false)),
            } );

            var expectedGuid = Guid.NewGuid();
            var expectedString = "example-map-getter";
            var obj = new MyClass() { ValueRef = expectedString, Value = expectedGuid };

            var actual = map.GetValue( "F1", obj );
            Assert.AreEqual( expectedString, actual );

            actual = map.GetValue( "F2", obj );
            Assert.AreEqual( expectedGuid, actual );

            actual = map.GetValue( "F3", obj );
            Assert.AreEqual( expectedString.Length, actual );

            obj.ValueRef = null;
            actual = map.GetValue( "F3", obj );
            Assert.AreEqual( 0, actual );
        }

        [TestMethod]
        public void MapSetterTest ( )
        {
            var map = new Map<MyClass>( new IMapping<MyClass>[]
            {
                new Mapping<MyClass>("ValueRef", "F1"),
                new Mapping<MyClass, Guid>("Value", "F2"),
                new Mapping<MyClass, int>("ValueRef.Length", "F3", true, new MappingSettings(true, false)),
            } );

            var expectedGuid = Guid.NewGuid();
            var expectedString = "example-map-getter";
            var obj = new MyClass();

            map.SetValue( "F1", obj, expectedString );
            Assert.AreEqual( expectedString, obj.ValueRef );

            map.SetValue( "F2", obj, expectedGuid );
            Assert.AreEqual( expectedGuid, obj.Value );

            var actual = map.GetValue( "F3", obj );
            Assert.AreEqual( expectedString.Length, actual );
        }

        [TestMethod]
        public void MapQueryMappingTest ( )
        {
            var expected = new IMapping<MyClass>[]
            {
                new Mapping<MyClass>("ValueRef", "F1"),
                new Mapping<MyClass>("Value", "F2"),
                new Mapping<MyClass, int>("ValueRef.Length", "F3", true, new MappingSettings(true, false)),
            };

            var map = new Map<MyClass>( expected );

            int i = 0;
            foreach ( var mapping in map.Mapping )
            {
                Assert.AreEqual( expected[i++], mapping, "Unexpected mapping." );
            }
        }
    }
}