﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PropertiesDynamicMapping;

namespace PropertiesDynamicMappingTest
{
    [TestClass]
    public class MappingSettingsTest
    {
        [TestMethod]
        public void CreationTest ( )
        {
            new MappingSettings( true, true );
            new MappingSettings( true, false );
            new MappingSettings( false, true );

            var exceptionText = "Invalid settings: should be set at least one property - mapGetter or mapSetter";

            try
            {
                new MappingSettings( false, false );
                Assert.Fail( "Exception expected: " + exceptionText );
            }
            catch ( Exception ex )
            {
                Assert.AreEqual( exceptionText, ex.Message );
            }
        }
    }
}